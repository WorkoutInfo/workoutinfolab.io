require 'bigdecimal'
require 'bigdecimal/util'

module Jekyll
  module StatisticsFilter
    def min(input)
      input.min { |a, b| a.to_d <=> b.to_d }
    end

    def max(input)
      input.max { |a, b| a.to_d <=> b.to_d }
    end

    def avg(input)
      input.reduce(0.0) { |sum, i| sum + i.to_d } / input.size
    end

    def median(input)
      sorted = input.map{ |n| n.to_f }.sort
      len = input.length
      len % 2 == 1 ? sorted[len/2] : (sorted[len/2 - 1] + sorted[len/2]) / 2
    end
  end

end

Liquid::Template.register_filter(Jekyll::StatisticsFilter)