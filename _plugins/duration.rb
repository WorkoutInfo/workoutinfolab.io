require 'ruby-duration'

module Jekyll
  module DurationFilter
    def duration(duration, format)

      hhmmss = /(\d*):(\d{2}):(\d{2})/
      if (duration =~ hhmmss )
        match = duration.match(hhmmss)
        duration = (60 * ((60 * Integer('0d'+match[1])) + Integer('0d'+match[2]))) + Integer('0d'+match[3])
      end

      # For `format` info see https://github.com/peleteiro/ruby-duration/blob/72e4b1bca6624fb594936ee3c1d81018062cd016/lib/duration.rb#L125-L152
      Duration.new(duration).format(format)
    end
  end
end

Liquid::Template.register_filter(Jekyll::DurationFilter)
