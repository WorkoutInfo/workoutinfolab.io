# Workout Program Information

The website is hosted at: [https://workoutinfo.gitlab.io/](https://workoutinfo.gitlab.io/)

## Contributing

Thank you for your interest!  Your contributions are very welcome!

If you just need to report an issue or ask a question, please create an issue using the [Issues](https://gitlab.com/WorkoutInfo/workoutinfo.gitlab.io/issues) link at the top of the page.

If you want to directly make a correction to existing content on the website, click on the appropriate `*.md` file in the [**_programs**](https://gitlab.com/WorkoutInfo/workoutinfo.gitlab.io/tree/master/_programs) folder.  Then click the **"Edit"** link.  You will be able to make the change and the website will guide you through making a "Merge Request" so your change can be accepted and published on the website.

If you have a new program you want to add, create a new file in the [**_programs**](https://gitlab.com/WorkoutInfo/workoutinfo.gitlab.io/tree/master/_programs) folder.  The easiest way to do this is to click [this link](https://gitlab.com/WorkoutInfo/workoutinfo.gitlab.io/new/master/_programs).  Then just copy the format of the existing files.  Once you create your merge request, the file will be automatically checked to make sure it correctly matches the format.

Finally, if you are more technically inclined, already know you can fork and clone this repository.  You are familiar with this, but not Jekyll, the instructions for _Running this website_ may be helpful.

## Running this website

You can contribute without running the website locally but doing so lets you preview your changes.

If you have [Bundler](http://bundler.io/) installed just enter the repository's directory and run:

```shell
bundle exec jekyll serve
```

The first time you may need to run `bundle install`.

### Dependencies
This website is build with [Jekyll](http://jekyllrb.com/) which is written in [Ruby](https://www.ruby-lang.org/en/).  If you have Ruby installed, you will need to install [Bundler](http://bundler.io/) via `gem install bundler`.