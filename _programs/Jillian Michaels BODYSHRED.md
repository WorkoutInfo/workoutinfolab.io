---
name: Jillian Michaels BODYSHRED
publisher: EM Productions, LLC.
product_url: https://www.jillianmichaelsbodyshred.com
timeRequired: P55D
episodes:
  - name: Amplify
    duration: 00:34:28
  - name: Apex
    duration: 00:34:29
  - name: Conquer
    duration: 00:34:24
  - name: Escalate
    duration: 00:33:47
  - name: Fire Up
    duration: 00:30:46
  - name: Ignite
    duration: 00:33:07
  - name: Launch
    duration: 00:33:41
  - name: Opus
    duration: 00:34:46
  - name: Rise
    duration: 00:35:41
  - name: Triumph
    duration: 00:35:14
  - name: Zenith
    duration: 00:35:33
...
