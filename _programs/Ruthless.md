---
name: Ruthless
publisher: Weider
product_url: https://www.weiderfitness.com/ruthless-workout-dvd
timeRequired: P60D
episodes:
  - name: Hardcore Circuit
    duration: 00:20:17
  - name: Lunatic Intervals
    duration: 00:18:57
  - name: Killer 100s
    duration: 00:20:09
  - name: Wicked Lower Body
    duration: 00:20:07
  - name: Fast-Slow Burn Circuit
    duration: 00:20:28
  - name: Ruthless Ringside
    duration: 00:21:11
  - name: TABATA Sweat
    duration: 00:21:07
  - name: Core & Flex
    duration: 00:20:02
  - name: Ruthless Give Back
    duration: 00:20:35
  - name: Pure Will Power
    duration: 00:19:45
  - name: Super Strength & Power
    duration: 00:22:18
  - name: Horizontal Circuit
    duration: 00:21:42
  - name: Nitro Burn
    duration: 00:21:00
  - name: Hardcore Abs & Arms
    duration: 00:22:44
  - name: DRIP
    duration: 00:21:15
  - name: Rip 10s
    duration: 00:20:42
  - name: Speed Power Sweat
    duration: 00:22:12
  - name: Performance Stretch & Yoga
    duration: 00:20:16
  - name: Total Body Circuit
    duration: 00:21:10
  - name: Partner Training
    duration: 00:20:05
...
