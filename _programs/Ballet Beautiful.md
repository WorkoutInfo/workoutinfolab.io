---
# TODO: File individual videos separately
name: Ballet Beautiful
publisher: Ballet Beauty, LLC
product_url: https://store.balletbeautiful.com/dvds-merchandise/classic-60-minute-workout-dvd-region-0
timeRequired: PT60M
episodes:
  - name: Classic 60-Minute Workout
    duration: 00:50:09
...
