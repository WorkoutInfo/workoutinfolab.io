---
name: Shaun T's Rockin' Body
product_url: https://www.beachbody.com/product/rockin_body.do
publisher: Beachbody, LLC
timeRequired: P27D
episodes:
  - name: Mark, Move and Groove
    duration: 00:14:41
  - name: Party Express
    duration: 00:25:41
  - name: Disco Groove
    duration: 00:35:17
  - name: Shaun T's Dance Party
    duration: 00:42:58
  - name: Rock it Out
    duration: 00:46:38
  - name: Hardcore Abs
    duration: 00:10:16
  - name: Booty Time
    duration: 00:28:41
...
