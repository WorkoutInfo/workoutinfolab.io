---
name: Georges St-Pierre RUSHFIT
product_url: http://gsprushfit.com
publisher: Pypeline Health Inc.
timeRequired: P55D
episodes:
  - name: Abdominal Strength and Core Conditioning
    warmup_duration: &warmup 00:10:52
    workout_duration: 00:30:23
    cooldown_duration: &cooldown 00:06:52
    duration: 00:48:07
  - name: Streching for Flexibility
    warmup_duration: *warmup
    workout_duration: 00:26:22
    cooldown_duration: *cooldown
    duration: 00:44:06
  - name: Balance & Agility
    warmup_duration: *warmup
    workout_duration: 00:23:59
    cooldown_duration: *cooldown
    duration: 00:41:43
  - name: The GSP Rushfit Assessment
    duration: 00:05:00
  - name: Explosive Powertraining
    warmup_duration: *warmup
    cooldown_duration: *cooldown
    workout_duration: 00:29:25
    duration: 00:47:09
  - name: Full Body Strength and Conditioning
    warmup_duration: *warmup
    cooldown_duration: *cooldown
    workout_duration: 00:29:28
    duration: 00:47:12
  - name: Strength and Endurance Workout
    warmup_duration: *warmup
    cooldown_duration: *cooldown
    workout_duration: 00:29:48
    duration: 00:47:32
  - name: The Fight Conditioning Workout
    warmup_duration: *warmup
    cooldown_duration: *cooldown
    workout_duration: 00:29:58
    duration: 00:47:42
...
