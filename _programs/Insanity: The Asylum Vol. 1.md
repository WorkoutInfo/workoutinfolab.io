---
name: "Insanity: The Asylum Vol. 1"
product_url: https://www.beachbody.com/productinsanity-next-level-asylum-workout.do
publisher: Beachbody, LLC
timeRequired: P30D
episodes:
  - name: Athletic Performance Assessment
    duration: 00:24:18
  - name: Back to Core
    duration: 00:42:31
  - name: Game Day
    duration: 01:00:51
  - name: Overtime
    duration: 00:13:10
  - name: Relief
    duration: 00:23:30
  - name: Speed & Agility
    duration: 00:45:15
  - name: Strength
    duration: 00:47:12
  - name: Vertical Pylo
    duration: 00:38:59
...
