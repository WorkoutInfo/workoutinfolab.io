---
name: Body Beast
$schema: ./fitness-program.schema.json
product_url: https://www.beachbody.com/product/body-beast-workout.do
publisher: Beachbody, LLC
timeRequired: P12W
episodes:
  - name: "Beast: Abs"
    duration: 00:10:32
  - name: "Beast: Cardio"
    duration: 00:30:09
  - name: "Beast: Total Body"
    duration: 00:38:36
  - name: "Build: Back/Bis"
    duration: 00:50:05
  - name: "Build: Chest/Tris"
    duration: 00:58:53
  - name: "Build: Legs"
    duration: 00:38:19
  - name: "Build: Shoulders"
    duration: 00:38:19
  - name: "Bulk: Arms"
    duration: 00:35:37
  - name: "Bulk: Back"
    duration: 00:29:05
  - name: "Bulk: Chest"
    duration: 00:30:07
  - name: "Bulk: Legs"
    duration: 00:41:14
  - name: "Bulk: Shoulders"
    duration: 00:35:20
  - name: "Lucky 7"
    duration: 00:22:38
  - name: "Tempo: Back/Bis"
    duration: 00:48:25
  - name: "Tempo: Chest/Tris"
    duration: 00:52:24
...