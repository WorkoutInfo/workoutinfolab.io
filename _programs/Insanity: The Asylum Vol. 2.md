---
name: "Insanity: The Asylum Vol. 2"
product_url: https://www.beachbody.com/product/insanity-the-asylum-volume-2-elite-training-workout.do
publisher: Beachbody, LLC
timeRequired: P4W
episodes:
  - name: Ab Shredder
    duration: 00:20:28
  - name: Back & 6 Pack
    duration: 00:37:43
  - name: Championship
    duration: 00:55:38
  - name: Off Day Stretch
    duration: 00:31:21
  - name: Power Legs
    duration: 00:49:44
  - name: Pure Contact
    duration: 00:22:22
  - name: Upper Elite
    duration: 01:00:06
  - name: X Trainer
    duration: 00:49:09
...