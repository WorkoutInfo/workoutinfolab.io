---
name: Tai Cheng
publisher: Beachbody, LLC
product_url: https://www.beachbody.com/product/tai-cheng-workout.do
timeRequired: P13W
episodes:
  - name: Bear And White Crane Combo
    duration: 00:06:55
  - name: Bear Comes Out Of Cave
    duration: 00:06:30
  - name: Brush Knee And Play The Pipa Combo
    duration: 00:08:22
  - name: Cross Hands
    duration: 00:08:57
  - name: Diagonal Punch Kick With Check & Punch
    duration: 00:07:50
  - name: Body Alignment For Better Living
    duration: 00:17:38
  - name: Breathing and Meditation
    duration: 00:03:40
  - name: Thi Cheng For Sport
    duration: 00:08:45
  - name: Thi Cheng For Travel
    duration: 00:09:00
  - name: Double Dutch Brush Knee & Push
    duration: 00:09:00
  - name: Full Sequence Fast
    duration: 00:09:00
  - name: Full Sequence Moderate
    duration: 00:09:00
  - name: Full Sequence Slow
    duration: 00:13:45
  - name: Lift & Lower Phoenix Tail Combo
    duration: 00:10:40
  - name: Neural Reboot 1
    duration: 00:18:20
  - name: Neural Reboot 2
    duration: 00:27:00
  - name: Neural Reboot 3
    duration: 00:26:45
  - name: Neural Reboot 4
    duration: 00:29:10
  - name: Phoenix Tail
    duration: 00:07:20
  - name: Play The Pipa
    duration: 00:06:10
  - name: Press And Separate & Pull Combo
    duration: 00:07:10
  - name: Press
    duration: 00:07:40
  - name: Pull Back
    duration: 00:07:11
  - name: Pulling Knee
    duration: 00:06:00
  - name: Push And Single Whip Combo
    duration: 00:09:00
  - name: Push
    duration: 00:07:10
  - name: Qi Gong Energizing Series
    duration: 00:07:14
  - name: Qi Gong Level 1
    duration: 00:10:34
  - name: Qi Gong Level 2
    duration: 00:08:38
  - name: Raise Hand And Pulling Knee Combo
    duration: 00:06:10
  - name: Raise Hand
    duration: 00:08:20
  - name: Separate & Pull
    duration: 00:07:10
  - name: Phase 1 Sequence
    duration: 00:12:50
  - name: Phase 2 Sequence
    duration: 00:06:50
  - name: Phase 3 Sequence
    duration: 00:12:20
  - name: Sheathing Sword And Diagonal Punch Kick With Check & Punch Combo
    duration: 00:08:07
  - name: Sheathing Sword
    duration: 00:07:55
  - name: Single Whip
    duration: 00:09:10
  - name: Standing Stances And Lift & Lower
    duration: 00:10:00
  - name: Ward Off
    duration: 00:07:00
  - name: Ward Off & Pull Back Combo
    duration: 00:07:00
  - name: White Crane Spreads Wings
    duration: 00:07:20
  - name: Withdraw & Seal And Cross Hands Combo
    duration: 00:08:43
  - name: Withdraw & Seal
    duration: 00:07:12
...