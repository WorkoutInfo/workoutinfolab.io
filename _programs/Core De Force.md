---
name: Core De Force
publisher: Beachbody, LLC
timeRequired: P30D
product_url: https://www.beachbody.com/product/core-de-force-mma-workout.do
episodes:
  - name: MMA Speed Workout
    duration: 00:26:00
  - name: MMA Shred Workout
    duration: 00:37:00
  - name: MMA Power Workout
    duration: 00:46:58
  - name: MMA Plyo
    duration: 00:46:58
  - name: Power Sculpt
    duration: 00:36:00
  - name: Dynamic Strength
    duration: 00:46:50
  - name: Active Recovery
    duration: 00:20:10
  - name: Core De Force Relief
    duration: 00:05:00
  - name: Core Kinetics
    duration: 00:15:30
  - name: MMA Mashup
    duration: 00:25:40
  - name: 5 Min. Core On The Floor
    duration: 00:05:00
...