---
name: Brazil Butt Lift
$schema: ./fitness-program.schema.json
product_url: https://www.beachbody.com/product/brazil_butt_lift.do
timeRequired: P56D
publisher: Beachbody, LLC
episodes:
  - name: Bum Bum
    duration: 00:29:42
  - name: Bum Bum Rapido
    duration: 00:10:31
  - name: Cardio Axe
    duration: 00:28:35
  - name: High & Tight
    duration: 00:37:50
  - name: Sculpt
    duration: 00:47:26
  - name: Tummy Tuck
    duration: 00:18:58
  - name: Rio Extreme
    duration: 00:50:14
  - name: Abs Rapido
    duration: 00:10:29
  - name: Upper Cuts
    duration: 00:12:14
  - name: Secret Weapon
    duration: 00:21:05
...