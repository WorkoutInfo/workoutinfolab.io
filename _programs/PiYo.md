---
name: PiYo
$schema: ./fitness-program.schema.json
product_url: https://www.beachbody.com/product/piyo-workout.do
publisher: Beachbody, LLC
timeRequired: P8W
episodes:
  - name: Align the Fundamentals
    duration: 00:42:04
  - name: Define Lower Body
    duration: 00:20:59
  - name: Define Upper Body
    duration: 00:18:48
  - name: Sweat
    duration: 00:36:51
  - name: Buns
    duration: 00:28:20
  - name: Core
    duration: 00:30:18
  - name: Strength Intervals
    duration: 00:21:52
  #- name: Bonus TurboFire Low HIIT 20
  #  duration: 00:18:59
  - name: Drench
    duration: 00:48:08
  - name: Sculpt
    duration: 00:26:40
  - name: Hardcore on the Floor
    duration: 00:33:30
  - name: Full Body Blast
    duration: 00:29:13
  - name: Strong Legs
    duration: 00:25:09
...