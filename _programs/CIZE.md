---
name: CIZE
product_url: https://www.beachbody.com/product/cize-dance-workout.do
publisher: Beachbody, LLC
timeRequired: P6W
episodes:
  - name: Crazy 8s
    workout_duration: 00:29:58
    cooldown_duration: 00:01:41
    duration: 00:31:47
  - name: You Got This
    workout_duration: 00:43:04
    cooldown_duration: 00:03:05
    duration: 00:46:17
  - name: Full Out
    workout_duration: 00:34:40
    cooldown_duration: 00:02:51
    duration: 00:37:39
  - name: In the Pocket
    workout_duration: 00:36:40
    cooldown_duration: 00:02:23
    duration: 00:39:11
  - name: Go for it
    workout_duration: 00:40:45
    cooldown_duration: 00:02:32
    duration: 00:43:25
  - name: Living in the 8s
    workout_duration: 00:51:41
    cooldown_duration: 00:04:12
    duration: 00:56:01
  - name: Eight-Count Abs
    duration: 00:08:42
...
