---
name: Focus T25
$schema: ./fitness-program.schema.json
product_url: https://www.beachbody.com/product/focus-t25-workout.do
timeRequired: P10W
publisher: Beachbody, LLC
episodes:
  - name: Ab Intervals
    duration: 00:28:50
  - name: Cardio
    duration: 00:27:43
  - name: Core Cardio
    duration: 00:27:47
  - name: Dynamic Core
    duration: 00:28:52
  - name: Lower Focus
    duration: 00:28:44
  - name: Rip't Circuit
    duration: 00:28:39
  - name: Speed 1.0
    duration: 00:27:42
  - name: Speed 2.0
    duration: 00:27:38
  - name: Stretch
    duration: 00:25:00
  - name: Total Body Circuit
    duration: 00:28:40
  - name: Upper Focus
    duration: 00:28:40
...