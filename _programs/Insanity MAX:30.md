---
name: Insanity MAX:30
publisher: Beachbody, LLC
product_url: https://www.beachbody.com/product/insanity-max-30-workout.do
timeRequired: P4W
episodes:
  - name: Ab Attack 10
    duration: 00:10:00
  - name: Cardio Challenge
    duration: 00:32:24
  - name: Friday Fight Round 1
    duration: 00:32:11
  - name: Friday Fight Round 2
    duration: 00:32:40
  - name: Max Out Cardio
    duration: 00:32:24
  - name: Max Out Power
    duration: 00:33:14
  - name: Max Out Strength
    duration: 00:32:38
  - name: Max Out Sweat
    duration: 00:32:42
  - name: Pulse
    duration: 00:20:00
  - name: Sweat Fest
    duration: 00:30:00
  - name: Sweat Intervals
    duration: 00:32:12
  - name: Tabata Power
    duration: 00:32:18
  - name: Tabata Strength
    duration: 00:32:03
...
