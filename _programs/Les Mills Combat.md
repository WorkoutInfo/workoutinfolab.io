---
name: Les Mill Combat
product_url: https://faq.beachbody.com/app/answers/detail/a_id/1148/lob/team
publisher: Beachbody, LLC
timeRequired: P60D
episodes:
  - name: Combat 30 - Kick Star
    duration: 00:30:01
  - name: Combat 45 - Power Kata
    duration: 00:43:13
  - name: Combat 60 Live - Ultimate Warrior's Workout
    duration: 00:53:10
  - name: Combat 60 - Extreme Cardio Fighter
    duration: 00:53:54
  - name: HIIT 1 - Power
    duration: 00:30:20
  - name: HIIT 2 - Shock Plyo
    duration: 00:28:33
  - name: Warrior 1 - Upper Body Blow Out
    duration: 00:24:28
  - name: Warrior 2 - Lower Body Lean Out
    duration: 00:29:08
  - name: Core Attack
    duration: 00:17:03
  - name: Inner Warrior - Stretch & Strength
    duration: 00:18:06
  - name: Combat 30 Live
    duration: 00:33:41
...