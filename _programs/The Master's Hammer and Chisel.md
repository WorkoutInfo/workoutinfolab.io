---
name: The Master's Hammer and Chisel
product_url: https://www.beachbody.com/product/master-hammer-chisel-workout.do
publisher: Beachbody, LLC
timeRequired: P8W
episodes:
  - name: 10 Min Ab Chisel
    duration: 00:09:51
  - name: 10 Min Ab Hammer
    duration: 00:11:41
  - name: Chisel Agility
    duration: 00:37:32
  - name: Chisel Balance
    duration: 00:40:12
  - name: Chisel Cardio
    duration: 00:37:59
  - name: Chisel Endurance
    duration: 00:36:02
  - name: Hammer Conditioning
    duration: 00:29:37
  - name: Hammer Plyometrics
    duration: 00:25:17
  - name: Hammer Power
    duration: 00:37:52
  - name: Iso Speed Hammer
    duration: 00:22:40
  - name: Iso Strength Chisel
    duration: 00:34:42
  - name: Max Hammer Strength
    duration: 00:35:31
  - name: The Master's Cardio
    duration: 00:16:47
  - name: Total Body Chisel
    duration: 00:35:07
  - name: Total Body Hammer
    duration: 00:42:41
...
