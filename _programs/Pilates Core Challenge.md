---
name: Pilates Core Challenge
product_url: http://gaiam.ca/Pilates-Core-Challenge-DVD-with-Ana-Caban
publisher: GAIAM
timeRequired: PT40M
episodes:
- name: Pilates Core Challenge
  duration: 00:38:00
...