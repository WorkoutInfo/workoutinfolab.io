---
name: The 21-Day Metashred
publisher: Rodale Inc.
product_url: http://www.21daymetashred.com/21daymetashred/index
timeRequired: P19D
episodes:
  - name: 5-Minute Death Sets
    duration: 00:30:14
  - name: Bodybuilder Burnouts
    duration: 00:30:03
  - name: Calorie Crushing Combos
    duration: 00:30:08
  - name: Density Doomsday
    duration: 00:30:17
  - name: Lightweight Leanout
    duration: 00:30:30
  - name: Metaconda
    duration: 00:31:50
  - name: Shrednado
    duration: 00:30:02
  - name: Six-Pack Superset
    duration: 00:30:12
  - name: Thermogenic Tempo Training
    duration: 00:30:08
...
