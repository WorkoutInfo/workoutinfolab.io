---
name: TurboFire
product_url: https://www.beachbody.com/product/turbofire.do
publisher: Beachbody, LLC
timeRequired: P139D
episodes:
  - name: Abs 10 Class
    duration: 00:10:38
  - name: Core 20 Class
    duration: 00:20:09
  - name: Fire 30 Class
    duration: 00:30:57
  - name: Fire 45 Class
    duration: 00:44:40
  - name: Fire 45 EZ Class
    duration: 00:43:41
  - name: Fire 55 EZ Class
    duration: 00:53:06
  - name: Fire 60 Class
    duration: 00:56:58
  - name: HIIT 15 Class
    duration: 00:16:27
  - name: HIIT 20 Class
    duration: 00:19:05
  - name: HIIT 25 Class
    duration: 00:24:33
  - name: HIIT 30 Class
    duration: 00:30:36
  - name: Lower 20 Class
    duration: 00:21:09
  - name: Sculpt 30 Class
    duration: 00:28:19
  - name: Stretch 10 Class
    duration: 00:12:29
  - name: Stretch 40 Class
    duration: 00:42:05
  - name: Tone 30 Class
    duration: 00:30:19
  - name: Upper 20 Class
    duration: 00:19:50
...
