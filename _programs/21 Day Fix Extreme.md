---
name: 21 Day Fix Extreme
$schema: ./fitness-program.schema.json
product_url: https://www.beachbody.com/product/21-day-fix-extreme.do
publisher: Beachbody, LLC
timeRequired: P3W
episodes:
  - name: 10 Minute Hardcore
    duration: 00:10:42
  - name: Abc Extreme
    duration: 00:32:14
  - name: Cardio Extreme
    duration: 00:31:21
  - name: Dirty 30 Extreme
    duration: 00:31:38
  - name: Lower Fix Extreme
    duration: 00:33:55
  - name: Pilates Extreme
    duration: 00:32:30
  - name: Plyo Fix Extreme
    duration: 00:30:15
  - name: Power Strength Extreme
    duration: 00:31:21
  - name: The Challenge
    duration: 00:28:57
  - name: Upper Extreme
    duration: 00:33:47
  - name: Yoga Extreme
    duration: 00:29:50
...