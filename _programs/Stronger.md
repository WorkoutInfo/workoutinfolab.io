---
name: Stronger
timeRequired: P55D
publisher: LIVESTRONG.COM
product_url: http://www.livestrong.com/article/1012121-10-workouts-fitter-stronger/
episodes:
  - name: Combustion
    duration: 00:34:04
  - name: Explosion
    duration: 00:34:12
  - name: Fire
    duration: 00:32:39
  - name: Ignition
    duration: 00:32:17
  - name: Iron
    duration: 00:33:24
  - name: Refuel
    duration: 00:40:26
  - name: Reload
    duration: 00:35:58
  - name: Soldier
    duration: 00:35:57
  - name: Steel
    duration: 00:37:47
  - name: Warrior
    duration: 00:35:09
...
