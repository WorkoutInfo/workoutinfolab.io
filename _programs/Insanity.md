---
name: Insanity
publisher: Beachbody, LLC
product_url: https://www.beachbody.com/product/insanity.do
timeRequired: P9W
episodes:
  - name: Fit Test
    duration: 00:25:39
  - name: Plyometric Cardio Circuit
    duration: 00:41:47
  - name: Cardio Power & Resistance
    duration: 00:38:19
  - name: Cardio Recovery
    duration: 00:33:02
  - name: Pure Cardio
    duration: 00:38:18
  - name: Cardio Abs
    duration: 00:18:45
  - name: Core Cardio & Balance
    duration: 00:37:19
  - name: Max Interval Circuit
    duration: 00:59:50
  - name: Max Interval Plyo
    duration: 00:55:11
  - name: Max Cardio Conditioning
    duration: 00:47:41
  - name: Max Recovery
    duration: 00:47:16
  - name: Insane Abs
    duration: 00:33:23
  - name: Max Interval Sports Training
    duration: 00:54:38
  - name: Upper Body Weight Training
    duration: 00:47:20
...