---
name: 21 Day Fix
$schema: "./fitness-program.schema.json"
product_url: https://www.beachbody.com/product/21-day-fix-simple-fitness-eating.do
publisher: Beachbody, LLC
timeRequired: P21D
episodes:
  - name: Total Body Cardio Fix
    duration: 00:28:40
  - name: Upper Fix
    duration: 00:31:44
  - name: Lower Fix
    duration: 00:29:53
  - name: Pilates Fix
    duration: 00:32:58
  - name: Cardio Fix
    duration: 00:31:57
  - name: Dirty 30
    duration: 00:27:41
  - name: Yoga Fix
    duration: 00:30:14
  - name: Plyo Fix
    duration: 00:31:21
  - name: 10-Minute Fix For Abs
    duration: 00:10:23
...