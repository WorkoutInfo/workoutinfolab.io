---
name: 22 Minute Hard Corps
$schema: "./fitness-program.schema.json"
product_url: https://www.beachbody.com/product/22-minute-hard-corps-workout.do
publisher: Beachbody, LLC
timeRequired: P8W
episodes:
  - name: Cardio 1
    duration: 00:21:41
  - name: Cardio 2
    duration: 00:22:21
  - name: Cardio 3
    duration: 00:22:00
  - name: Cold Start
    duration: 00:10:24
  - name: Core 1
    duration: 00:09:22
  - name: Core 2
    duration: 00:11:23
  - name: Resistance 1
    duration: 00:21:49
  - name: Resistance 2
    duration: 00:21:49
  - name: Resistance 3
    duration: 00:11:52
  - name: Special Ops Cardio
    duration: 00:22:01
  - name: Special Ops Core
    duration: 00:16:34
  - name: Special Ops Resistance
    duration: 00:21:55
...