---
name: Power 90 Master Series
product_url: https://faq.beachbody.com/app/answers/detail/a_id/5072/~/power-90-master-series%3A-retired/lob/team
publisher: Beachbody, LLC
timeRequired: P12W
episodes:
  - name: Cardio Intervals
    duration: 00:54:28
  - name: Core Cardio
    duration: 00:48:30
  - name: Plyo Legs
    duration: 00:50:58
  - name: Sculpt 5/6
    duration: 00:42:53
  - name: Sweat 5/6
    duration: 00:44:39
  - name: Upper Middle Lower
    duration: 00:38:34
...