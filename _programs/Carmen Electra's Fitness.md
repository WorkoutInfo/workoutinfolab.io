---
# TODO: File videos without a schedule separately
name: Carmen Electra's Aerobic Striptease
publisher: DNA Studio
product_url: https://www.amazon.com/Carmen-Electras-Aerobic-Striptease-Collection/dp/B0007TKH98
timeRequired: P0D
episodes:
  - name: Aerobic Striptease
    Routine 1 Instruction: 00:11:35
    Routine 1 Run Through: 00:08:14
    Routine 2 Instruction: 00:11:07
    Routine 2 Run Through: 00:02:08
    Routine 3 Instruction: 00:12:21
    Routine 3 Run Through: 00:02:34
    duration: 00:47:59
  - name: Fit To Strip
    Cardio Warmup: 00:10:01
    Floor Work: 00:19:52
    "Isolation: Lower Body": 00:09:45
    "Isolation: Abs": 00:05:26
    "Isolation: Upper Body": 00:07:35
    duration: 00:52:39
  - name: Advanced Aerobic Striptease
    duration: 00:00:00
  - name: "Aerobic Striptease: In The Bedroom"
    # These should probably be listed as separate workouts
    Warm Up and Toning: 00:32:06
    Routine: 00:39:20
    duration: 00:71:26
  - name: The Lap Dance
    Lap Dance Instruction: Unknown
    duration: 00:00:00
  - name: Hip Hop
    Instruction: 00:14:11
    Run Through: 00:01:42
    duration: 00:15:53
...