---
name: Hit Hop Abs
$schema: ./fitness-program.schema.json
product_url: https://www.beachbody.com/product/hip_hop_abs.do
publisher: Beachbody, LLC
timeRequired: P27D
episodes:
  - name: Last Minute Abs
    duration: '00:05:14'
  - name: Ab Sculpt
    duration: '00:23:15'
  - name: Fat Burning Cardio
    duration: '00:29:44'
  - name: 'Hips, Buns, and Thighs'
    duration: '00:24:07'
  - name: Total Body Burn
    duration: '00:41:30'
...
