---
name: Power 90
product_url: http://www.teambeachbody.com/watch/products/power-90
publisher: Beachbody, LLC
timeRequired: P90D
episodes:
  - name: Ab Ripper 100
    duration: 00:05:21
  - name: Ab Ripper 200
    duration: 00:07:00
  - name: Fat Burner Express
    duration: 00:35:50
  - name: Sculpt Circut 1-2
    duration: 00:28:44
  - name: Sculpt Circut 3-4
    duration: 00:38:00
  - name: Sweat Cardio 1-2
    duration: 00:41:20
  - name: Sweat Cardio 3-4
    duration: 00:48:40
...