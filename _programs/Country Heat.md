---
name: Country Heat
publisher: Beachbody, LLC
timeRequired: P30D
product_url: https://www.beachbody.com/product/country-heat-dance-workout.do
episodes:
  - name: Bring the Heat - Breakdown
    duration: 00:14:16
  - name: Bring the Heat - Routine
    duration: 00:26:54
  - name: Country Swing - Breakdown
    duration: 00:22:08
  - name: Country Swing - Routine
    duration: 00:31:44
  - name: Dance Conditioning
    duration: 00:23:24
  - name: Down and Dirty - Breakdown
    duration: 00:14:59
  - name: Down and Dirty - Routine
    duration: 00:26:28
  - name: Giddy Up - Breakdown
    duration: 00:19:27
  - name: Giddy Up - Routine
    duration: 00:28:07
  - name: Night Crawl - Breakdown
    duration: 00:07:26
  - name: Night Crawl - Routine
    duration: 00:02:43
  - name: Trail Ride - Breakdown
    duration: 00:17:13
  - name: Trail Ride - Routine
    duration: 00:24:52
...
