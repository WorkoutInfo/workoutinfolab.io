---
name: RevAbs
product_url: https://www.beachbody.com/product/revabs_club.do
publisher: Beachbody, LLC
timeRequired: P90D
episodes:
  - name: Fire Up Your Abs
    duration: 00:39:50
  - name: Power Intervals 1
    duration: 00:27:40
  - name: Total Strength
    duration: 00:44:05
  - name: Fat Burning Abs
    duration: 00:38:48
  - name: Power Intervals 2
    duration: 00:31:47
  - name: Strength & Endurance
    duration: 00:37:28
  - name: Merciless Abs
    duration: 00:14:59
  - name: Rev It Up Cardio
    duration: 00:43:17
...