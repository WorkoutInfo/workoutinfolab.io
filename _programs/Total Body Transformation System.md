---
# TODO: File videos without a schedule separately
name: Total Body Transformation System
product_url: https://www.amazon.com/Zumba-Fitness-Total-Transformation-System/dp/B002HZ4XMC
publisher: Zumba Fitness, LLC
timeRequired: P0D
episodes:
  - name: Basics
    duration: 00:56:58
#  - name: 20-Minute Express
#    duration: 
  - name: Sculpt & Tone
    duration: 00:44:24
  - name: Cardio Party
    duration: 00:46:00
  - name: Live!
    duration: 00:51:44
  - name: Flat Abs
    duration: 00:17:22
...