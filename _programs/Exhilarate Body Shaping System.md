---
name: Exhilarate Body Shaping System
product_url: https://www.zumba.com/en-US/shop/US/product/zumba-exhilarate-7-dvd-set-d0d00121
publisher: Zumba Fitness, LLC
timeRequired: P10D
episodes:
  - name: Step By Step
    duration: 00:59:15
  - name: Rush
    duration: 00:22:45
  - name: Activate
    duration: 00:40:51
  - name: Toning
    duration: 01:01:57
    equipment:
      - chair
  - name: Exhilarate
    duration: 00:58:45
  - name: Mix
    duration: 01:31:14
  - name: Fitness Concert
    duration: 01:00:22
...
