---
name: UFC Fit
product_url: http://ufcfit.com/
publisher: Zuffa Evolution Fit, LLC
timeRequired: P96D
episodes:
  - name: Power Punch
    duration: 00:41:47
  - name: Power Pull
    duration: 00:41:57
  - name: Plyo Shred
    duration: 00:39:07
  - name: Fat Fighter
    duration: 00:33:51
  - name: Striker Strength
    duration: 00:45:57
  - name: Grapple Strength
    duration: 00:42:24
  - name: Ultimate Fit Challenger
    duration: 00:40:11
  - name: Ultimate Fit Champion
    duration: 00:34:41
  - name: Cardio Cross Train
    duration: 00:41:37
  - name: Ab Assassin
    duration: 00:23:13
  - name: Ultimate Stretch Flex
    duration: 00:20:18
  - name: Shark Attack
    duration: 00:30:43
...
