---
name: 10-Minute Trainer
$schema: "./fitness-program.schema.json"
product_url: https://www.beachbody.com/product/10_minute_trainer.do
publisher: Beachbody, LLC
timeRequired: P28D
episodes:
  - name: Abs
    duration: 00:12:07
  - name: Cardio
    duration: 00:13:32
  - name: Core Cardio
    duration: 00:11:46
  - name: Lower Body
    duration: 00:11:24
  - name: Total Body 2
    duration: 00:13:46
  - name: Total Body
    duration: 00:14:16
  - name: Upper Body
    duration: 00:11:44
  - name: Yoga Flex
    duration: 00:12:00
...