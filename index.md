---
layout: default
---

# Workout Program Information. Simple.

No referral links.  No upselling or other marketing.  Just information about different home workout programs and videos.  See the [All Programs](programs.html) link at the top of the page to get started.

## Why?

Fitness is important and finding the right program to meet your needs should be as easy as possible.  There are other resources with reviews and opinions on workout programs, but if you just want quick answers about time or equipment to help find the programs that could work for you; you are in the right place.

Additionally, when you have strict time requirements it can be hard to find the right workout to fit your schedule.  Here you can find accurate information on how long you will need to complete a workout.  No more squeezing in the time for a workout and running out of time because the official cool down was not included in the advertised time.

## How it works

Information on each program or video has been manually entered by a person like you and then automatically processed to show on this website.  If you have suggestions, additions, or corrections, you can help out by reporting them. [Information on how to do this can be found in this project's GitLab repository](https://gitlab.com/WorkoutInfo/workoutinfo.gitlab.io).

### Times

Workout times aim to reflect the actual time needed to complete the workout.  This may be different from the count down display timer in the video (if applicable).  For example, if the workout clock is running during an introduction before the workout starts, that time is not included here.  Similarly, sometimes a workout has separate clocks for warmup, workout, and cool down; Here, only the total time is shown.